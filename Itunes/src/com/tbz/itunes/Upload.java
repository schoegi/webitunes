package com.tbz.itunes;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Upload {
	
	 public static void main(String[] args) throws FileNotFoundException {
	        new Upload().start();
	    }
	
	
	 public void start() throws FileNotFoundException {

	        String fileName = null;
	        final CountDownLatch latch = new CountDownLatch(1);
	        SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                latch.countDown();
	            }
	        });
	        try {
	            latch.await();
	        } catch (InterruptedException ex) {
	            Logger.getLogger(Upload.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        JFileChooser chooser = new JFileChooser();
	        FileNameExtensionFilter filter = new FileNameExtensionFilter(
	                "MP3 Files", "mp3");
	        chooser.setFileFilter(filter);
	        int returnVal = chooser.showOpenDialog(null);
	        if (returnVal == JFileChooser.APPROVE_OPTION) {
	        	fileName = chooser.getSelectedFile().getName();

	        }
	        
	        File saveMusic = new File("C:\\itunesWeb\\" + fileName);
	        chooser.getSelectedFile().renameTo(saveMusic);
	}



}
